module Gist
  class Contact
    class << self
      def create_or_update_contact(payload: nil)
        Gist::Client.connect(payload: payload, method: :post, endpoint: "contacts").contact
      end

      def get_contact(id: nil, user_id: nil, email: nil)
        raise "Only use one parameter" if [id, user_id, email].compact.count > 1
        if id
          Gist::Client.connect(method: :get, endpoint: "contacts/#{id}").contact
        elsif user_id
          Gist::Client.connect(method: :get, endpoint: "contacts", params: {user_id: user_id}).contact
        elsif email
          Gist::Client.connect(method: :get, endpoint: "contacts", params: {email: email}).contact
        end
      end

      def delete_contact(id: nil)
        Gist::Client.connect(method: :delete, endpoint: "contacts/#{id}")
      end
    end
  end
end