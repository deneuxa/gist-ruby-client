module Gist
  class Team
    class << self
      def list_team(page: 1, per_page: 50)
        params = {
                page: page,
                per_page: per_page
        }
        Gist::Client.connect(params: params, method: :get, endpoint: "teams").teams
      end

      def get_team(id: nil)
        Gist::Client.connect(method: :get, endpoint: "teams/#{id}").team
      end
    end
  end
end