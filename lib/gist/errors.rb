module Gist
	class StandardError < ::StandardError
		def initialize(status:, description:)
			@status = status
			@description = description
		end

		def to_h
		{
			status: status,
			description: description,
		}
		end

		def serializable_hash
			to_h
		end

		def to_s
			to_h.to_s
		end

		attr_reader :status, :description
	end

	class BadRequest < Gist::StandardError; end
	class InvalidAPIKey < Gist::StandardError; end
	class Forbidden < Gist::StandardError; end
	class NotFound < Gist::StandardError; end
	class MethodNotAllowed < Gist::StandardError; end
	class UnprocessableEntity < Gist::StandardError; end
	class TooManyRequests < Gist::StandardError; end
    class BadGateway < Gist::StandardError; end
end
