# frozen_string_literal: true

module Gist
  class Conversation
    class << self
      # a l'air obsolète : semble ne renvoyer que les contacts non-assignés
      # def list_conversations(order: "desc", order_by: 'updated_at', status: "open", page: 1, per_page: 50)
      #   params = {
      #     order: order,
      #     order_by: order_by,
      #     status: status,
      #     page: page,
      #     per_page: per_page
      #   }
      #   Gist::Client.connect(params: params, method: :get, endpoint: "conversations")
      # end

      def search_conversations(order: "desc", order_by: 'updated_at', assignee_filter: true, is_assigned: true, assignee_id: nil, assignee_type: 'team', contact_id: nil, tag_id: nil, status: "open", page: 1, per_page: 50)
        params = {
          order: order,
          order_by: order_by,
          status: status,
          page: page,
          per_page: per_page,
        }

        filter_query = []
        if assignee_filter
          if assignee_id && is_assigned
            filter_query << { criteria: [{ key: "#{assignee_type}_assigned_id", operator: (assignee_id.is_a?(Array) ? 'IN' : '='), value: assignee_id }] }
          else
            filter_query << { criteria: [{ key: 'is_assigned', operator: '=', value: is_assigned }] }
          end
        end
        if contact_id
          filter_query << { criteria: [{ key: 'contact.id', operator: (contact_id.is_a?(Array) ? 'IN' : '='), value: contact_id }] }
        end
        if tag_id
          filter_query << { criteria: [{ key: 'conversation_tags', operator: (tag_id.is_a?(Array) ? 'IN' : '='), value: tag_id }] }
        end
        payload = { filter_query: filter_query }
        Gist::Client.connect(params: params, payload: payload, method: :post, endpoint: "conversations/search")
      end

      def create_conversation(user_id: nil, email: nil, id: nil, body: nil)
        #html is not supported inside the body
        payload = {
          from: {
            id: id,
            user_id: user_id,
            email: email
          },
          body: body
        }
        Gist::Client.connect(payload: payload, method: :post, endpoint: "conversations").conversation
      end

      def update_conversation(id: nil, title: nil, custom_properties: [])
        raise "Conversation ID is missing" unless id
        payload = {}
        payload.merge!({ title: title }) if title
        payload.merge!({ custom_properties: custom_properties }) if custom_properties.any?
        Gist::Client.connect(payload: payload, method: :patch, endpoint: "conversations/#{id}").conversation
      end

      def get_conversation(id: nil)
        raise "Conversation ID is missing" unless id
        Gist::Client.connect(method: :get, endpoint: "conversations/#{id}").conversation
      end

      def delete_conversation(id: nil)
        raise "Conversation ID is missing" unless id
        Gist::Client.connect(method: :delete, endpoint: "conversations/#{id}")
      end

      def add_tag(id: nil, message_id: nil, tags: nil)
        raise "Conversation ID is missing" unless id
        payload = { message_id: message_id,
                    tags: tags }
        Gist::Client.connect(payload: payload, method: :post, endpoint: "conversations/#{id}/tags")
      end

      def remove_tag(id: nil, message_id: nil, tag_id: nil)
        raise "Conversation ID is missing" unless id
        payload = { message_id: message_id,
                    tag_id: tag_id }
        Gist::Client.connect(payload: payload, method: :delete, endpoint: "conversations/#{id}/tags")
      end

      def assign_conversation(assignee_id: nil, teammate_id: nil, id: nil, assignee_type: 'teammate')
        raise "Conversation ID is missing" unless id
        payload = {
          teammate_id: teammate_id,
          assignee_id: assignee_id,
          assignee_type: assignee_type
        }
        Gist::Client.connect(payload: payload, method: :patch, endpoint: "conversations/#{id}/assign").conversation
      end

      def snooze_conversation(snooze_until: nil, teammate_id: nil, id: nil)
        raise "Conversation ID is missing" unless id
        payload = {
          teammate_id: teammate_id,
          state: "snoozed",
          snooze_until: snooze_until
        }
        Gist::Client.connect(payload: payload, method: :patch, endpoint: "conversations/#{id}").conversation
      end

      def reopen_conversation(teammate_id: nil, id: nil)
        raise "Conversation ID is missing" unless id
        payload = {
          teammate_id: teammate_id,
          state: "open"
        }
        Gist::Client.connect(payload: payload, method: :patch, endpoint: "conversations/#{id}").conversation
      end

      def close_conversation(teammate_id: nil, id: nil)
        raise "Conversation ID is missing" unless id
        payload = {
          teammate_id: teammate_id,
          state: "closed"
        }
        Gist::Client.connect(payload: payload, method: :patch, endpoint: "conversations/#{id}").conversation
      end

      def prioritize_conversation(priority: "priority", teammate_id: nil, id: nil)
        raise "Conversation ID is missing" unless id
        payload = {
          teammate_id: teammate_id,
          priority: priority
        }
        Gist::Client.connect(payload: payload, method: :patch, endpoint: "conversations/#{id}/priority").conversation
      end

      def retrieve_conversation_count
        Gist::Client.connect(method: :get, endpoint: "conversations/count").conversation_count
      end

      def retrieve_conversation_messages(id: nil, per_page: 50, order_by: 'created_at', order: 'desc', with_notes: true)
        raise "Conversation ID is missing" unless id
        params = {
          order: order,
          order_by: order_by,
          per_page: per_page,
          with_notes: with_notes
        }
        Gist::Client.connect(params: params, method: :get, endpoint: "conversations/#{id}/messages").messages
      end

      def reply_to_contact(user_id: nil, email: nil, message_type: "reply", id: nil, body: nil, conversation_id: nil)
        payload = {
          message_type: message_type,
          from: {
            id: id,
            user_id: user_id,
            email: email,
            type: contact
          },
          body: body
        }
        Gist::Client.connect(payload: payload, method: :post, endpoint: "conversations/#{conversation_id}/messages").message
      end

      def reply_to_teammate(message_type: "reply", teammate_id: nil, body: nil, conversation_id: nil)
        payload = {
          message_type: message_type,
          from: {
            type: "teammate",
            teammate_id: teammate_id
          },
          body: body
        }
        Gist::Client.connect(payload: payload, method: :post, endpoint: "conversations/#{conversation_id}/messages").message
      end
    end
  end
end