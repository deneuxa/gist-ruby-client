# frozen_string_literal: true

module Gist
  class Client
    API_URL = 'https://api.getgist.com'
    class << self
      attr_accessor :access_token, :read_timeout, :open_timeout

      def connect(payload: {}, params: {}, method: nil, endpoint: nil)
        uri = URI.parse("#{API_URL}/#{endpoint}")
        uri.query = URI.encode_www_form(params)

        http_method = case method
                      when :get
                        Net::HTTP::Get
                      when :post
                        Net::HTTP::Post
                      when :patch
                        Net::HTTP::Patch
                      when :delete
                        Net::HTTP::Delete
                      else
                        raise 'invalid method'
                      end

        request = http_method.new(uri.request_uri, {'Content-Type' => 'application/json', 'Authorization' => "Bearer #{@access_token}"})
        request.body = payload.to_json if payload.present?

        # Créez une instance de Net::HTTP
        http = Net::HTTP.new(uri.host, uri.port)
        http.use_ssl = uri.scheme == 'https'
        http.verify_mode = OpenSSL::SSL::VERIFY_PEER

        # Configurez les délais d'expiration
        http.open_timeout = @open_timeout || 5 # Délai d'expiration en secondes pour établir la connexion
        http.read_timeout = @read_timeout || 5 # Délai d'expiration en secondes pour lire la réponse

        response = http.request(request)
        response_body = response.body || '{}'

        return JSON.parse(response_body, object_class: OpenStruct) if %w(200 204).include?(response.code)
        #in case of error the body is not always a json
        description = begin
          JSON.parse(response_body)
        rescue
          response_body
        end
        context = {status: response.code, description: description}
        case response.code
        when '400'
          raise Gist::BadRequest.new(**context)
        when '401'
          raise Gist::InvalidAPIKey.new(**context)
        when '403'
          raise Gist::Forbidden.new(**context)
        when '404'
          raise Gist::NotFound.new(**context)
        when '405'
          raise Gist::MethodNotAllowed.new(**context)
        when '422'
          raise Gist::UnprocessableEntity.new(**context)
        when '429'
          raise Gist::TooManyRequests.new(**context)
        when '502'
          raise Gist::BadGateway.new(**context)
        else
          raise Gist::StandardError.new(**context)
        end
      end
    end
  end
end
