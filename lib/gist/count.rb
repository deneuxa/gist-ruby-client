module Gist
  class Count
    class << self
      def conversations
        Gist::Client.connect(method: :get, endpoint: "conversations/count").conversation_count
      end

      def conversations_by_team
        Gist::Client.connect(method: :get, endpoint: "conversations/count/teams").conversation_count.teams
      end

      def conversations_by_teammate
        Gist::Client.connect(method: :get, endpoint: "conversations/count/teammates").conversation_count.teammates
      end
    end
  end
end