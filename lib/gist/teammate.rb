module Gist
  class Teammate
    class << self
      def list_teammates(page: 1, per_page: 50)
        params = {
                page: page,
                per_page: per_page
        }
        Gist::Client.connect(params: params, method: :get, endpoint: "teammates").teammates
      end

      def get_teammate(id: nil)
        Gist::Client.connect(method: :get, endpoint: "teammates/#{id}").teammate
      end
    end
  end
end